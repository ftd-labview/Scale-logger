# Sartorius Scale Logger

A simple LabVIEW datalogger for two Sartorius scales using serial ports and the SBI protocol.

## License
This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact
For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
